package com.example.myapplication;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

public class BluetoothConectionService {
    private static final BluetoothConectionService ourInstance = new BluetoothConectionService();
    private static final String TAG = "BluetoothConnectionServ";

    private static final String appName = "UniDepo";

    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    private final BluetoothAdapter myBluetoothAdapter;
    Context myContext;

    private AcceptThread mInsecureAcceptThread;
    private AcceptThread.ConnectThread mConnectThread;
    private BluetoothDevice myDevice;
    private UUID deviceUUID;
    ProgressDialog mProgressDialog;

    private ConnectedThrad mConnecteThread;

    public BluetoothConectionService(BluetoothAdapter myBluetoothAdapter, Context myContext) {
    this.myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    this.myContext = myContext;
    }

    private class AcceptThread extends Thread {
      private final BluetoothServerSocket myserverSocket;

      public AcceptThread(){
        BluetoothServerSocket tmp  = null;
        try{
            tmp = myBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appName, MY_UUID_INSECURE);
            Log.d(TAG, "AcceptThread:Setting up Server using:" + MY_UUID_INSECURE);
        }catch(IOException e){
          Log.e(TAG,"AccepThread :IOExpection:"+ e.getMessage());
        }


        myserverSocket=tmp;
      }
      public void run(){
        Log.d(TAG,"run: AcceptThread Running.");
        BluetoothSocket socket = null;

        try {
            Log.d(TAG, "run: RFCOM server socket start .....");
            socket = myserverSocket.accept();
        }catch (IOException e ){
          Log.e(TAG,"AccepThread :IOExpection:"+ e.getMessage());
      }
      if (socket!=null){
        connected(socket,myDevice);
      }
      Log.i(TAG,"End myAcceptThread");

    }
    public void cancel(){
        Log.d(TAG,"Cancel:Canceling AcceptThread.");
        try{
          myserverSocket.close();
        }catch (IOException e){
          Log.e(TAG,"Cancel:Close of AcceptThread ServerSocketfaild"+e.getMessage());
        }
      }


      private class  ConnectThread extends Thread {
        private BluetoothSocket mySocket;

        public ConnectThread(BluetoothDevice device, UUID uuid){
          Log.d(TAG,"ConnectThread:  Started.  ");
          myDevice = device;
          deviceUUID = uuid;
        }
        public void run(){
            BluetoothSocket tmep =  null;
            Log.i(TAG,"Run mConnectThread");
          try {
            Log.d(TAG,"ConnectThread: Trying to creat InsecureRfcommSocket using UUID");
            temp = myDevice.createInsecureRfcommSocketToServiceRecord(deviceUUID);
          }catch (IOException e ){
            Log.e(TAG,"ConeectionThrad: Could not create InsecurefcommSocket"+e.getMessage());
          }
          mySocket = temp;

          myBluetoothAdapter.cancelDiscovery();

          try {
            mySocket.connect();

            Log.d(TAG,"run:ConnectThread connected.");
          } catch (IOException e) {
            try {
              mySocket.close();
              Log.d(TAG,"run:Closed Scoket");
            } catch (IOException e1) {
              Log.e(TAG,"mConnectThread :run:Unable to close connection in socket "+e1.getMessage());
            }
            Log.d(TAG,"run:ConnectThread:Could not connect to UUID:",MY_UUID_INSECURE);

          }
          connected(mySocket,myDevice);
        }
        public void cancel(){
          Log.d(TAG,"Cancel:Canceling AcceptThread.");
          try{
            myserverSocket.close();
          }catch (IOException e){
            Log.e(TAG,"Cancel:Close of AcceptThread ServerSocketfaild"+e.getMessage());
          }
        }
      }
      public synchronized void start(){
        Log.d(TAG,"start");

        if(mConnectThread !=null){
          mConnectThread.cancel();
          mConnectThread =null;
        }
        if (mInsecureAcceptThread == null){
          mInsecureAcceptThread = new AcceptThread();
          mInsecureAcceptThread.start();
        }
      }
      public void starClient(BluetoothDevice device, UUID uuid){
        Log.d(TAG,"startClient:Started");

        mProgressDialog = ProgressDialog.show(myContext,"Connecting Bluetooth","Please Wait...",true);
        mConnectThread = new ConnectThread(device,uuid);
        mConnectThread.start();
      }
      public static class ConnectedThread extends Thread {
        private final BluetoothSocket mySocket;
        private final OutputStream myOutStream;
        private final InputStream myInputStream;
        public ConnectThread(BluetoothSocket socket){
          Log.d(TAG,"ConnectThread:Starting.");
          mySocket=socket;
          InputStream tmpIn = null;
          OutputStream tmpOut = null;

          mProgressDialog.dismiss();
          try {
            tmpIn= mySocket.getInputStream();
            tmpOut=mySocket.getOutputStream();
          } catch (IOException e) {
            e.printStackTrace();
          }

          myInputStream=tmpIn;
          myOutStream=tmpOut;

        }
        public void  run(){
          byte[] buffer = new byte[1024];
          int bytes;

          while(true){
            try {
              bytes=myInputStream.read(buffer);
              String incomingMessage = new String(buffer,0,bytes);
              Log.d(TAG,"InputStream:"+incomingMessage);
            } catch (IOException e) {
              Log.e(TAG,"Write:Error reading to inputstream"+e.getMessage());
              break;
            }

          }

        }
        public void write(byte[] bytes){
          String text = new String(bytes, Charset.defaultCharset());
          Log.d(TAG , "write:Writing to outputstream:"+text);
          try {
            myOutStream.write(bytes);
          } catch (IOException e) {
            Log.e(TAG,"Write:Error writting to outputstream"+e.getMessage());
          }
        }


        public void cancel(){
          Log.d(TAG,"Cancel:Canceling AcceptThread.");
          try{
            myserverSocket.close();
          }catch (IOException e){
            Log.e(TAG,"Cancel:Close of AcceptThread ServerSocketfaild"+e.getMessage());
          }
        }


      }
    }

  private void connected(BluetoothSocket mySocket, BluetoothDevice myDevice) {
      Log.d(TAG,"Connected:Starting.");

      mConnecteThread = new AcceptThread.ConnectedThread(mySocket);
      mConnecteThread.start();
  }
  public void write(byte[] out){
    AcceptThread.ConnectedThread r = null;
  Log.d(TAG,"write:Write Called.");
  r.write();


  }


  public static BluetoothConectionService getInstance() {
        return ourInstance;
    }

    private BluetoothConectionService() {
    }
}
